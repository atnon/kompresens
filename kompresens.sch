EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:Screw_Terminal_01x04 J1
U 1 1 60D5B2BE
P 2050 3200
F 0 "J1" H 2130 3192 50  0000 L CNN
F 1 "Screw_Terminal_01x04" H 2130 3101 50  0000 L CNN
F 2 "TerminalBlock_RND:TerminalBlock_RND_205-00014_Pitch5.00mm" H 2050 3200 50  0001 C CNN
F 3 "~" H 2050 3200 50  0001 C CNN
	1    2050 3200
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2250 3100 2350 3100
Wire Wire Line
	2350 3100 2350 3300
Wire Wire Line
	2350 3300 2250 3300
Wire Wire Line
	2250 3200 2450 3200
Wire Wire Line
	2450 3200 2450 3400
Wire Wire Line
	2450 3400 2250 3400
$Comp
L Diode:1N4148 D2
U 1 1 60D64CD1
P 3250 3000
F 0 "D2" H 3250 2783 50  0000 C CNN
F 1 "1N4148" H 3250 2874 50  0000 C CNN
F 2 "Diodes_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 3250 2825 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 3250 3000 50  0001 C CNN
F 4 "2675146" H 3250 3000 50  0001 C CNN "Farnell SKU"
F 5 "Multicomp" H 3250 3000 50  0001 C CNN "Manufacturer Name"
F 6 "1N4148 (DO-35)" H 3250 3000 50  0001 C CNN "Manufacturer Part number"
	1    3250 3000
	-1   0    0    1   
$EndComp
$Comp
L Diode:1N4148 D3
U 1 1 60D68288
P 3500 3250
F 0 "D3" V 3454 3330 50  0000 L CNN
F 1 "1N4148" V 3545 3330 50  0000 L CNN
F 2 "Diodes_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 3500 3075 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 3500 3250 50  0001 C CNN
F 4 "2675146" H 3500 3250 50  0001 C CNN "Farnell SKU"
F 5 "Multicomp" H 3500 3250 50  0001 C CNN "Manufacturer Name"
F 6 "1N4148 (DO-35)" H 3500 3250 50  0001 C CNN "Manufacturer Part number"
	1    3500 3250
	0    1    1    0   
$EndComp
Wire Wire Line
	3400 3000 3500 3000
Wire Wire Line
	3500 3000 3500 3100
$Comp
L Isolator:4N25 U1
U 1 1 60D6BF7A
P 4800 3250
F 0 "U1" H 4800 3575 50  0000 C CNN
F 1 "4N25" H 4800 3484 50  0000 C CNN
F 2 "Housings_DIP:DIP-6_W7.62mm" H 4600 3050 50  0001 L CIN
F 3 "https://www.vishay.com/docs/83725/4n25.pdf" H 4800 3250 50  0001 L CNN
F 4 "1612453" H 4800 3250 50  0001 C CNN "Farnell SKU"
F 5 "VISHAY" H 4800 3250 50  0001 C CNN "Manufacturer Name"
F 6 "4N25" H 4800 3250 50  0001 C CNN "Manufacturer Part number"
	1    4800 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	2350 3100 2350 3000
Wire Wire Line
	2350 3000 3000 3000
Connection ~ 2350 3100
Wire Wire Line
	2450 3400 2450 3500
Wire Wire Line
	2450 3500 3000 3500
Wire Wire Line
	3500 3500 3500 3400
Connection ~ 2450 3400
Wire Wire Line
	3500 3500 4250 3500
Wire Wire Line
	4250 3500 4250 3350
Wire Wire Line
	4250 3350 4500 3350
Connection ~ 3500 3500
Wire Wire Line
	3500 3000 3750 3000
Wire Wire Line
	4250 3000 4250 3150
Wire Wire Line
	4250 3150 4500 3150
Connection ~ 3500 3000
$Comp
L Device:R R1
U 1 1 60D714FD
P 3900 3000
F 0 "R1" V 3693 3000 50  0000 C CNN
F 1 "4.7k" V 3784 3000 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0411_L9.9mm_D3.6mm_P15.24mm_Horizontal" V 3830 3000 50  0001 C CNN
F 3 "~" H 3900 3000 50  0001 C CNN
F 4 "MCF 0.5W 4K7" V 3900 3000 50  0001 C CNN "Manufacturer Part number"
F 5 "9338829" V 3900 3000 50  0001 C CNN "Farnell SKU"
F 6 "Multicomp" H 3900 3000 50  0001 C CNN "Manufacturer Name"
	1    3900 3000
	0    1    1    0   
$EndComp
Wire Wire Line
	4050 3000 4250 3000
Text Label 2550 3000 0    50   ~ 0
24VAC_P
Text Label 2550 3500 0    50   ~ 0
24VAC_N
$Comp
L Device:C C1
U 1 1 60D72F4C
P 5500 3250
F 0 "C1" H 5615 3296 50  0000 L CNN
F 1 "1u" H 5615 3205 50  0000 L CNN
F 2 "Capacitors_THT:C_Disc_D5.1mm_W3.2mm_P5.00mm" H 5538 3100 50  0001 C CNN
F 3 "~" H 5500 3250 50  0001 C CNN
F 4 "2860102" H 5500 3250 50  0001 C CNN "Farnell SKU"
F 5 "VISHAY" H 5500 3250 50  0001 C CNN "Manufacturer Name"
F 6 "K105K20X7RF5TH5" H 5500 3250 50  0001 C CNN "Manufacturer Part number"
	1    5500 3250
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 60D7410F
P 5500 2750
F 0 "R2" H 5430 2704 50  0000 R CNN
F 1 "200k" H 5430 2795 50  0000 R CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5430 2750 50  0001 C CNN
F 3 "~" H 5500 2750 50  0001 C CNN
F 4 "9341501" H 5500 2750 50  0001 C CNN "Farnell SKU"
F 5 "" H 5500 2750 50  0001 C CNN "Manufacturer part number"
F 6 "Multicomp" H 5500 2750 50  0001 C CNN "Manufacturer Name"
F 7 "MF25 200K" H 5500 2750 50  0001 C CNN "Manufacturer Part number"
	1    5500 2750
	-1   0    0    1   
$EndComp
Wire Wire Line
	5100 3250 5200 3250
Wire Wire Line
	5200 3250 5200 3000
Wire Wire Line
	5200 3000 5500 3000
Wire Wire Line
	5500 3000 5500 3100
Wire Wire Line
	5100 3350 5200 3350
Wire Wire Line
	5200 3350 5200 3500
Wire Wire Line
	5200 3500 5500 3500
Wire Wire Line
	5500 3500 5500 3400
Wire Wire Line
	5500 3000 5500 2900
Connection ~ 5500 3000
Wire Wire Line
	5500 2600 5500 2550
$Comp
L power:+3V3 #PWR0101
U 1 1 60D78CE3
P 5500 2500
F 0 "#PWR0101" H 5500 2350 50  0001 C CNN
F 1 "+3V3" H 5515 2673 50  0000 C CNN
F 2 "" H 5500 2500 50  0001 C CNN
F 3 "" H 5500 2500 50  0001 C CNN
	1    5500 2500
	1    0    0    -1  
$EndComp
Text Notes 1000 3800 0    50   ~ 0
24VAC in with through-connection for connection to other things
$Comp
L power:GND #PWR0102
U 1 1 60D79E94
P 5500 3600
F 0 "#PWR0102" H 5500 3350 50  0001 C CNN
F 1 "GND" H 5505 3427 50  0000 C CNN
F 2 "" H 5500 3600 50  0001 C CNN
F 3 "" H 5500 3600 50  0001 C CNN
	1    5500 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	5500 3600 5500 3500
Connection ~ 5500 3500
$Comp
L Device:D_TVS D1
U 1 1 60D7B1FB
P 3000 3250
F 0 "D1" V 2954 3330 50  0000 L CNN
F 1 "1.5KE43A 36.8Vwm" V 2850 2900 50  0000 L CNN
F 2 "Diodes_THT:D_DO-201_P12.70mm_Horizontal" H 3000 3250 50  0001 C CNN
F 3 "~" H 3000 3250 50  0001 C CNN
F 4 "2679630" H 3000 3250 50  0001 C CNN "Farnell SKU"
F 5 "LITTELFUSE" H 3000 3250 50  0001 C CNN "Manufacturer Name"
F 6 "1.5KE43A" H 3000 3250 50  0001 C CNN "Manufacturer Part number"
	1    3000 3250
	0    1    1    0   
$EndComp
Wire Wire Line
	3000 3400 3000 3500
Connection ~ 3000 3500
Wire Wire Line
	3000 3500 3500 3500
Wire Wire Line
	3000 3000 3000 3100
Connection ~ 3000 3000
Wire Wire Line
	3000 3000 3100 3000
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 60D7ED42
P 4600 1200
F 0 "#FLG0101" H 4600 1275 50  0001 C CNN
F 1 "PWR_FLAG" H 4600 1373 50  0000 C CNN
F 2 "" H 4600 1200 50  0001 C CNN
F 3 "~" H 4600 1200 50  0001 C CNN
	1    4600 1200
	-1   0    0    1   
$EndComp
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 60D7FF7E
P 4150 1200
F 0 "#FLG0102" H 4150 1275 50  0001 C CNN
F 1 "PWR_FLAG" H 4150 1373 50  0000 C CNN
F 2 "" H 4150 1200 50  0001 C CNN
F 3 "~" H 4150 1200 50  0001 C CNN
	1    4150 1200
	-1   0    0    1   
$EndComp
$Comp
L power:PWR_FLAG #FLG0103
U 1 1 60D80AB8
P 3250 1200
F 0 "#FLG0103" H 3250 1275 50  0001 C CNN
F 1 "PWR_FLAG" H 3250 1373 50  0000 C CNN
F 2 "" H 3250 1200 50  0001 C CNN
F 3 "~" H 3250 1200 50  0001 C CNN
	1    3250 1200
	-1   0    0    1   
$EndComp
$Comp
L power:+3.3V #PWR0103
U 1 1 60D8751D
P 3250 1100
F 0 "#PWR0103" H 3250 950 50  0001 C CNN
F 1 "+3.3V" H 3265 1273 50  0000 C CNN
F 2 "" H 3250 1100 50  0001 C CNN
F 3 "" H 3250 1100 50  0001 C CNN
	1    3250 1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 1200 4150 1000
Wire Wire Line
	4150 1000 5000 1000
Wire Wire Line
	4600 1200 4600 1100
Wire Wire Line
	4600 1100 5000 1100
Text Label 4600 1000 0    50   ~ 0
24VAC_P
Text Label 4600 1100 0    50   ~ 0
24VAC_N
Wire Wire Line
	3250 1100 3250 1200
$Comp
L power:PWR_FLAG #FLG0104
U 1 1 60D8BD33
P 3650 1100
F 0 "#FLG0104" H 3650 1175 50  0001 C CNN
F 1 "PWR_FLAG" H 3650 1273 50  0000 C CNN
F 2 "" H 3650 1100 50  0001 C CNN
F 3 "~" H 3650 1100 50  0001 C CNN
	1    3650 1100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 60D8C7D0
P 3650 1200
F 0 "#PWR0104" H 3650 950 50  0001 C CNN
F 1 "GND" H 3655 1027 50  0000 C CNN
F 2 "" H 3650 1200 50  0001 C CNN
F 3 "" H 3650 1200 50  0001 C CNN
	1    3650 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 1100 3650 1200
NoConn ~ 5100 3150
$Comp
L Connector_Generic:Conn_01x10 J2
U 1 1 60D932CA
P 6600 3250
F 0 "J2" H 6680 3242 50  0000 L CNN
F 1 "Conn_01x10" H 6680 3151 50  0000 L CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x10_Pitch2.54mm" H 6600 3250 50  0001 C CNN
F 3 "~" H 6600 3250 50  0001 C CNN
	1    6600 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5500 3500 5950 3500
Wire Wire Line
	5950 3500 5950 3750
Wire Wire Line
	5950 3750 6400 3750
Wire Wire Line
	6400 3650 6050 3650
Wire Wire Line
	6050 3650 6050 2550
Wire Wire Line
	6050 2550 5500 2550
Connection ~ 5500 2550
Wire Wire Line
	5500 2550 5500 2500
Wire Wire Line
	6400 3450 6150 3450
Wire Wire Line
	6150 3450 6150 3000
Wire Wire Line
	6150 3000 5500 3000
Text Label 5600 3000 0    50   ~ 0
PWR_SENS
NoConn ~ 6400 3550
NoConn ~ 6400 3350
NoConn ~ 6400 3250
NoConn ~ 6400 3150
NoConn ~ 6400 3050
NoConn ~ 6400 2950
NoConn ~ 6400 2850
$EndSCHEMATC
